/**
 * Copyright (C) 2016-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Videocut.
 *
 * Videocut is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Videocut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Videocut.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.videocut;

import java.io.IOException;

import fr.devinsy.util.cmdexec.CmdExecException;

/**
 * The Class VideoCut.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class VideoCut
{
    /**
     * The main method.
     * 
     * @param args
     *            the arguments
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws CmdExecException
     *             the cmd exec exception
     */
    public static void main(final String[] args) throws IOException, CmdExecException
    {
        if (args.length == 0)
        {
            System.out.println("Videocut, a hugly hashing cutter for video files.");
            System.out.println("Usage: videocut [videofile]");
            System.out.println("Please, give a video file as parameter.");
        }
        else
        {
            VideoCutGUI.showGUI(args);
        }
    }
}
