/**
 * Copyright (C) 2016-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Videocut.
 *
 * Videocut is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Videocut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Videocut.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.videocut;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;

/**
 * The Class VideoCutGUI.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class VideoCutGUI extends JFrame
{
    private static final long serialVersionUID = 4095070877471599882L;

    private JPanel contentPane;
    private VideoData data;
    private JTextField txtfldPositions;
    private JTextField txtfldFile;
    private JTextField txtfldStatus;

    /**
     * Create the frame.
     * 
     * @param source
     *            the source
     */
    public VideoCutGUI(final File source)
    {
        setIconImage(Toolkit.getDefaultToolkit().getImage(VideoCutGUI.class.getResource("/fr/devinsy/videocut/videocut-logo.png")));
        addKeyListener(new KeyAdapter()
        {
            /**
             * 
             */
            @Override
            public void keyTyped(final KeyEvent event)
            {
                try
                {
                    System.out.println("[char,code,mod]=[" + event.getKeyChar() + "," + event.getKeyCode() + "," + event.getModifiers() + "]");

                    if (event.getModifiers() == 0)
                    {
                        //
                        switch (event.getKeyChar())
                        {
                        // Start keys.
                            case 'a':
                                VideoCutGUI.this.data.incStart(-VideoData.HUNDRED_MEGA);
                            break;

                            case 'z':
                                VideoCutGUI.this.data.incStart(VideoData.HUNDRED_MEGA);
                            break;

                            case 'e':
                                VideoCutGUI.this.data.incStart(-VideoData.TEN_MEGA);
                            break;

                            case 'r':
                                VideoCutGUI.this.data.incStart(VideoData.TEN_MEGA);
                            break;

                            case 't':
                                VideoCutGUI.this.data.incStart(-VideoData.ONE_MEGA);
                            break;

                            case 'y':
                                VideoCutGUI.this.data.incStart(VideoData.ONE_MEGA);
                            break;

                            // End keys.
                            case 'q':
                                VideoCutGUI.this.data.incEnd(-VideoData.HUNDRED_MEGA);
                            break;

                            case 's':
                                VideoCutGUI.this.data.incEnd(VideoData.HUNDRED_MEGA);
                            break;

                            case 'd':
                                VideoCutGUI.this.data.incEnd(-VideoData.TEN_MEGA);
                            break;

                            case 'f':
                                VideoCutGUI.this.data.incEnd(VideoData.TEN_MEGA);
                            break;

                            case 'g':
                                VideoCutGUI.this.data.incEnd(-VideoData.ONE_MEGA);
                            break;

                            case 'h':
                                VideoCutGUI.this.data.incEnd(VideoData.ONE_MEGA);
                            break;

                            // Control keys.
                            case 'o':
                            {
                                String command = String.format("tail -c +%d %s | mplayer -geometry 1024x768 -", VideoCutGUI.this.data.getStart(), VideoCutGUI.this.data.getEscapedFileName());
                                System.out.println("command=[" + command + "@]");
                                String log;

                                log = CmdExec.run("bash", "-c", command);

                                System.out.println("log=[" + log + "]");
                            }
                            break;

                            case 'p':
                            {
                                String command = String.format("tail -c +%d %s | mplayer -geometry 1024x768 -", VideoCutGUI.this.data.getEnd(), VideoCutGUI.this.data.getEscapedFileName());
                                String log = CmdExec.run("bash", "-c", command);

                                System.out.println("log=[" + log + "]");
                            }
                            break;

                            case 'm':
                            {
                                VideoCutGUI.this.txtfldStatus.setText("Making");
                                String command = String.format("head -c %d %s | tail -c +%d > t", VideoCutGUI.this.data.getEnd(), VideoCutGUI.this.data.getEscapedFileName(),
                                        VideoCutGUI.this.data.getStart());
                                String log = CmdExec.run("bash", "-c", command);

                                VideoCutGUI.this.txtfldStatus.setText("Made");
                                System.out.println("log=[" + log + "]");
                            }
                            break;

                            case ',':
                            {
                                String command = String.format("vlc t");
                                String log = CmdExec.run("bash", "-c", command);

                                System.out.println("log=[" + log + "]");
                            }
                            break;

                            case '!':
                            {
                                String command = String.format("mv t %s; md5sum %s > %s.md5", VideoCutGUI.this.data.getEscapedFileName(), VideoCutGUI.this.data.getEscapedFileName(),
                                        VideoCutGUI.this.data.getEscapedFileName());
                                String log = CmdExec.run("bash", "-c", command);

                                System.out.println("log=[" + log + "]");
                            }
                            break;
                        }

                    }

                    //
                    refreshPositions();

                }
                catch (CmdExecException exception)
                {
                    exception.printStackTrace();
                }
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 700, 400);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("File");
        mnFile.setMnemonic('F');
        menuBar.add(mnFile);

        JMenuItem mntmQuit = new JMenuItem("Quit");
        mntmQuit.addActionListener(new ActionListener()
        {
            /**
             * 
             */
            @Override
            public void actionPerformed(final ActionEvent event)
            {
                System.exit(0);
            }
        });
        mntmQuit.setMnemonic('Q');
        mnFile.add(mntmQuit);

        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(this.contentPane);
        this.contentPane.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
                FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

        JLabel lblFile = new JLabel("File:");
        lblFile.setFocusable(false);
        this.contentPane.add(lblFile, "2, 2, right, default");

        this.txtfldFile = new JTextField();
        this.txtfldFile.setFocusable(false);
        this.txtfldFile.setEditable(false);
        this.contentPane.add(this.txtfldFile, "4, 2, fill, default");
        this.txtfldFile.setColumns(10);

        JLabel lblPositions = new JLabel("Positions:");
        lblPositions.setFocusable(false);
        this.contentPane.add(lblPositions, "2, 4, right, default");

        this.txtfldPositions = new JTextField();
        this.txtfldPositions.setFocusable(false);
        this.txtfldPositions.setEditable(false);
        this.contentPane.add(this.txtfldPositions, "4, 4, fill, default");
        this.txtfldPositions.setColumns(10);

        JLabel lblStatus = new JLabel("Status:");
        this.contentPane.add(lblStatus, "2, 6, right, default");

        this.txtfldStatus = new JTextField();
        this.txtfldStatus.setFocusable(false);
        this.txtfldStatus.setEditable(false);
        this.txtfldStatus.setColumns(10);
        this.contentPane.add(this.txtfldStatus, "4, 6, fill, default");

        // /////////////////////////////////
        this.data = new VideoData(source);
        this.txtfldFile.setText(this.data.getSource().getName());

        JLabel lblCommands = new JLabel("Keyboard actions:");
        this.contentPane.add(lblCommands, "4, 10");

        JLabel lblStartPointA = new JLabel("     start point: a z (-/+ 100 MB), e r (-/+ 10 MB), t y (-/+ 1 MB)");
        this.contentPane.add(lblStartPointA, "4, 12");

        JLabel lblEndPointQ = new JLabel("     end point:   q s (-/+ 100 MB), d f (-/+ 10 MB), g h (-/+ 1 MB)");
        this.contentPane.add(lblEndPointQ, "4, 14");

        JLabel lblH = new JLabel("     show from start point:   o");
        this.contentPane.add(lblH, "4, 16");

        JLabel lblShowFromEnd = new JLabel("     show from end point:     p");
        this.contentPane.add(lblShowFromEnd, "4, 18");

        JLabel lblNewLabel = new JLabel("     extract from start point to end point:   m");
        this.contentPane.add(lblNewLabel, "4, 20");
        refreshPositions();
    }

    /**
     * 
     */
    private void refreshPositions()
    {
        this.txtfldPositions.setText(this.data.toString());
    }

    /**
     * Launch the application.
     * 
     * @param args
     *            the args
     */
    public static void showGUI(final String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    VideoCutGUI frame = new VideoCutGUI(new File(args[0]));
                    frame.setVisible(true);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
        });
    }
}
