/**
 * Copyright (C) 2016-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Videocut.
 *
 * Videocut is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Videocut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Videocut.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.videocut;

import java.io.File;

/**
 * The Class VideoData.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class VideoData
{
    public static final long ONE_MEGA = 1024 * 1024;
    public static final long TEN_MEGA = 10 * 1024 * 1024;
    public static final long HUNDRED_MEGA = 100 * 1024 * 1024;

    private String escapedFileName;
    private File source;
    private long start;
    private long end;

    /**
     * Instantiates a new video data.
     * 
     * @param source
     *            the source
     */
    public VideoData(final File source)
    {
        this.source = source;
        this.escapedFileName = escape(source.getAbsolutePath());
        this.start = 0;
        this.end = source.length();
    }

    /**
     * Gets the end.
     * 
     * @return the end
     */
    public long getEnd()
    {
        return this.end;
    }

    /**
     * Gets the escaped file name.
     * 
     * @return the escaped file name
     */
    public String getEscapedFileName()
    {
        return this.escapedFileName;
    }

    /**
     * Gets the source.
     * 
     * @return the source
     */
    public File getSource()
    {
        return this.source;
    }

    /**
     * Gets the start.
     * 
     * @return the start
     */
    public long getStart()
    {
        return this.start;
    }

    /**
     * Inc end.
     * 
     * @param value
     *            the value
     */
    public void incEnd(final long value)
    {
        this.end += value;

        if (this.end < this.start)
        {
            this.end = this.start + ONE_MEGA;
        }

        if (this.end < 0)
        {
            this.end = ONE_MEGA;
        }
        else if (this.end > this.source.length())
        {
            this.end = this.source.length();
        }
    }

    /**
     * Inc start.
     * 
     * @param value
     *            the value
     */
    public void incStart(final long value)
    {
        this.start += value;

        if (this.start > this.end)
        {
            this.start = this.end - ONE_MEGA;
        }

        if (this.start < 0)
        {
            this.start = 0;
        }
        else if (this.start > this.source.length())
        {
            this.start = this.source.length() - ONE_MEGA;
        }
    }

    /**
     * Sets the end.
     * 
     * @param end
     *            the new end
     */
    public void setEnd(final long end)
    {
        this.end = end;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final File source)
    {
        this.source = source;
    }

    /**
     * Sets the start.
     * 
     * @param start
     *            the new start
     */
    public void setStart(final long start)
    {
        this.start = start;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[%,dMB ; %,dMB ] %,dMB / %,dMB", this.start / 1024, this.end / 1024, (this.end - this.start) / 1024, this.source.length() / 1024);

        //
        return result;
    }

    /**
     * Escape.
     * 
     * @param source
     *            the source
     * @return the string
     */
    public static String escape(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = source.replace(" ", "\\ ").replace(",", "\\,").replace("'", "\\'").replace("(", "\\(").replace(")", "\\)");
        }

        //
        return result;
    }
}
