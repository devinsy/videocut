/**
 * Copyright (C) 2016-2018 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Videocut.
 *
 * Videocut is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Videocut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Videocut.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.videocut;

import java.io.File;
import java.io.IOException;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;

/**
 * The Class VideoCut.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class VideoCutCLI
{
    /**
     * Run CLI.
     * 
     * @param args
     *            the args
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws CmdExecException
     *             the cmd exec exception
     */
    public static void runCLI(final String[] args) throws IOException, CmdExecException
    {
        if (args.length == 1)
        {
            System.out.println("source=" + args[0]);

            VideoData data = new VideoData(new File(args[0]));
            boolean ended = false;
            while (!ended)
            {
                System.out.println(data);

                char c = (char) System.in.read();
                System.in.read();

                System.out.println(c);

                switch (c)
                {
                // Start keys.
                    case 'a':
                        data.incStart(-VideoData.HUNDRED_MEGA);
                    break;

                    case 'z':
                        data.incStart(VideoData.HUNDRED_MEGA);
                    break;

                    case 'e':
                        data.incStart(-VideoData.TEN_MEGA);
                    break;

                    case 'r':
                        data.incStart(VideoData.TEN_MEGA);
                    break;

                    case 't':
                        data.incStart(-VideoData.ONE_MEGA);
                    break;

                    case 'y':
                        data.incStart(VideoData.ONE_MEGA);
                    break;

                    // End keys.
                    case 'q':
                        data.incEnd(-VideoData.HUNDRED_MEGA);
                    break;

                    case 's':
                        data.incEnd(VideoData.HUNDRED_MEGA);
                    break;

                    case 'd':
                        data.incEnd(-VideoData.TEN_MEGA);
                    break;

                    case 'f':
                        data.incEnd(VideoData.TEN_MEGA);
                    break;

                    case 'g':
                        data.incEnd(-VideoData.ONE_MEGA);
                    break;

                    case 'h':
                        data.incEnd(VideoData.ONE_MEGA);
                    break;

                    // Control keys.
                    case 'o':
                    {
                        String command = String.format("tail -c +%d %s | mplayer -geometry 1024x768 -", data.getStart(), data.getEscapedFileName());
                        System.out.println("command=[" + command + "@]");
                        String log = CmdExec.run("bash", "-c", command);

                        System.out.println("log=[" + log + "]");
                    }
                    break;

                    case 'p':
                    {
                        String command = String.format("tail -c +%d %s | mplayer -geometry 1024x768 -", data.getEnd(), data.getEscapedFileName());
                        String log = CmdExec.run("bash", "-c", command);

                        System.out.println("log=[" + log + "]");
                    }
                    break;

                    case 'm':
                    {
                        String command;

                        command = String.format("head -c %d %s | tail -c +%d > t", data.getEnd(), data.getEscapedFileName(), data.getStart());
                        String log = CmdExec.run("bash", "-c", command);

                        System.out.println("log=[" + log + "]");
                    }
                    break;

                    case '0':
                        ended = true;
                    break;
                }
            }

            System.out.println("Done.");
        }
        else
        {
            System.out.println("Usage: videocut videofilename");
        }
    }
}
