  
# Videocut

Videocut is a hugly hashing cutter for video files.

## Author
Christian Pierre MOMON &lt;christian.momon@devinsy.fr&gt;

## License
Videocut is released under the GNU AGPL license.

## Requirements

Videocut requires:
- Java 1.6

## Logo

Logo origin:
- source: https://pixabay.com/en/cut-scissors-trimmer-clippers-tool-97585/
- license: CC0 Creative Commons

## Conclusion
Enjoy and use FlatDB4GeoNames. For questions, improvement, issues: christian.momon@devinsy.fr
    